"use strict";
let gulp = require("gulp");
let ts = require('gulp-typescript');
let del = require("del");
let changed = require("itay-gulp-changed");
let merge = require("merge2");

let config = {
    tsconfig: "./src/tsconfig.json",
    tsGlob: "./src/**/*.ts",
    dest: "./lib",
};

gulp.task("build", function () {
    let tsProject = ts.createProject(config.tsconfig, { declaration: true });

    let tsResult = gulp.src(config.tsGlob)
        .pipe(changed())
        .pipe(tsProject());

    tsResult.on("error", () => {
        changed.reset();
        throw "Typescript build failed.";
    });

    return merge([
        tsResult.js.pipe(gulp.dest(config.dest)),
        tsResult.dts.pipe(gulp.dest(config.dest)),
    ]);
});

gulp.task("clean", () => del([config.dest, "./.localStorage"]));