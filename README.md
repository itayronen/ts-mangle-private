# ts-mangle-private
Gulp plugin - Mangle private members in typescript files.

## Why?
* Smaller files.
* Harder to reverse engineer.
* uglifyjs does not mangle typescript.
* uglifyjs does not mangle private members in javascript because it cannot know which members are private.
* uglifyjs has a mangle properties option but using it is a mess.

## install
`npm install --save-dev ts-mangle-private`

## Usage
```ts
let gulp = require("gulp");
let ts = require('gulp-typescript');
let manglePrivate = require("ts-mangle-private").default;
// Or: import manglePrivate from "ts-mangle-private";

let tsProject = ts.createProject("./src/tsconfig.json");

return gulp.src("./src/**/*.ts")
	.pipe(manglePrivate())
	.pipe(tsProject())
	.pipe(gulp.dest("./lib"));
```