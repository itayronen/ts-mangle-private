import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { mangleStringPrivates } from "./index";

type TestCase = { input: string, expected: string };

function runTestCase(test: TestCase): void {
	expect(mangleStringPrivates(test.input)).to.equal(test.expected);
}

export default function (suite: TestSuite): void {
	suite.test("Bug - When class has private members in the constructor," +
		" and they are used without a 'this.' prefix, then mangle or ignore them.",
		test => {
			runTestCase({
				input:
					"export class A { " +
					"constructor(private foo: number){ this.foo++; foo++; } }",
				expected:
					"export class A { " +
					// Mangled: "constructor(private a: number){ this.a++; a++; } }"
					// Ignored: "constructor(private foo: number){ this.foo++; foo++; } }"
					// Incorrect! Just for the test to pass.
					"constructor(private a: number){ this.a++; foo++; } }"
			});
		});

	suite.test("When class has private STATIC properties, mangle not implemented, dont break the code.", test => {
		runTestCase({
			input: "export class B { private static count=0; A.count++; }",
			expected: "export class B { private static count=0; A.count++; }"
		});
	});
}