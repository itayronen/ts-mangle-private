import ObjectStream, { EnteredArgs, Transform } from "o-stream";
import * as File from "vinyl";

type Member = { search: RegExp, replacement: string };
type Members = Member[];
type ClassIndexes = { firstLineIndex: number, lastLineIndex: number };

export default function plugin(): Transform {
	return ObjectStream.transform({
		onEntered: (args: EnteredArgs<File, File>) => {
			let mangled = mangleStringPrivates(args.object.contents!.toString());
			args.object.contents = new Buffer(mangled);

			args.output.push(args.object);
		}
	});
}

export function mangleStringPrivates(content: string): string {
	let lines = content!.toString().split(/([\n;{}])/g);

	let classes: ClassIndexes[] = getClassesIndexes(lines);

	for (let c of classes) {
		if (isDerivingClass(c, lines)) continue;

		let privateMembers = manglePrivates(lines, c.firstLineIndex, c.lastLineIndex);
		replaceAllMemberAccess(privateMembers, lines, c.firstLineIndex, c.lastLineIndex);
	}

	return lines.join("");
}

function getClassesIndexes(lines: string[]): ClassIndexes[] {
	let result: ClassIndexes[] = [];
	let last: ClassIndexes | undefined;

	for (let i = 0; i < lines.length; i++) {
		let line = lines[i];

		if (line.match(/^\s*(export\s+)?class\s+/)) {
			if (last) {
				last.lastLineIndex = i - 1;
			}

			last = { firstLineIndex: i, lastLineIndex: i };

			result.push(last);
		}
	}

	if (last) {
		last.lastLineIndex = lines.length - 1;
	}

	return result;
}

function isDerivingClass(c: ClassIndexes, lines: string[]) {
	return lines[c.firstLineIndex].match(/\s+extends\s+/);
}

function replaceAllMemberAccess(members: Members, lines: string[], firstIndex: number, lastIndex: number): void {
	members.forEach(member => {
		replaceMemberAccess(member, lines, firstIndex, lastIndex);
	});
}

function replaceMemberAccess(member: Member, lines: string[], firstIndex: number, lastIndex: number): void {
	for (let i = firstIndex; i <= lastIndex; i++) {
		let line = lines[i];

		line = line.replace(member.search, member.replacement);

		lines[i] = line;
	}
}

function manglePrivates(lines: string[], firstIndex: number, lastIndex: number): Members {
	let privateMembers: Members = [];
	let availableCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	let mangleIndex = 0;
	let regEx = /private\s+(?!constructor|static)((async|readonly)\s+)?\w+/g;

	for (let i = firstIndex; i <= lastIndex; i++) {
		let line = lines[i];

		let execution = regEx.exec(line);

		while (execution) {
			let match: string = execution[0]

			let split = match.split(/\s+/);
			let isAsync = split[1] == "async";
			let isReadonly = split[1] == "readonly";
			let member = isAsync || isReadonly ? split[2] : split[1];

			let mangleString = availableCharacters[mangleIndex++];

			privateMembers.push({
				search: new RegExp(`this.${member}(?!\\w)`, "g"),
				replacement: `this.${mangleString}`
			});

			let newDefinition = "private " +
				(isAsync ? "async " : isReadonly ? "readonly " : "") +
				mangleString;

			line = line.replace(match, newDefinition);

			execution = regEx.exec(line);
		}

		lines[i] = line;
	}

	return privateMembers;
}