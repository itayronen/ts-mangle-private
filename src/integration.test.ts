import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import ObjectStream, { EnteredArgs, EndedArgs, Transform } from "o-stream";
import * as gutil from "gulp-util";
import manglePrivates, { mangleStringPrivates } from "./index";

type TestCase = { input: string, expected: string };

export default function (suite: TestSuite): void {
	suite.test("Simple integration test.", async test => {
		await runIntegrationTestCase({
			input: "export class A { private foo; private count=0; " +
				"constructor(){ this.foo = new Foo(); this.count++; } }",
			expected: "export class A { private a; private b=0; " +
				"constructor(){ this.a = new Foo(); this.b++; } }"
		});
	});
}

function runTestCase(test: TestCase): void {
	expect(mangleStringPrivates(test.input)).to.equal(test.expected);
}

function runIntegrationTestCase(test: TestCase): Promise<void> {
	return new Promise<void>((resolve, reject) => {
		let file = new gutil.File({ contents: new Buffer(test.input) });
		let stream = ObjectStream.fromArray([file]);

		stream.pipe(manglePrivates()).pipe(ObjectStream.transform({
			onEntered: (args: EnteredArgs<gutil.File, void>) => {
				try {
					expect(args.object.contents!.toString()).to.equal(test.expected)
				}
				catch (reason) { reject(reason); }
			},
			onEnded: () => { resolve(); }
		}));
	});
}
