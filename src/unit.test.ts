import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { mangleStringPrivates } from "./index";

type TestCase = { input: string, expected: string };

function runTestCase(test: TestCase): void {
	expect(mangleStringPrivates(test.input)).to.equal(test.expected);
}

export default function (suite: TestSuite): void {
	suite.test("When class has multiple private properties, mangle them.", test => {
		runTestCase({
			input: "export class A { private foo; private count=0; " +
				"constructor(){ this.foo = new Foo(); this.count++; } }",
			expected: "export class A { private a; private b=0; " +
				"constructor(){ this.a = new Foo(); this.b++; } }"
		});
	});

	suite.test("When class private constructor, don't mangle it.", test => {
		runTestCase({
			input:
				"export class A {" +
				"private constructor(private foo: number){ this.foo++; } }",
			expected:
				"export class A {" +
				"private constructor(private a: number){ this.a++; } }",
		});
	});

	suite.test("When module has imports and a class, then mangles the class.", test => {
		runTestCase({
			input: "import * as sheker from \"sheker\"; " +
				"export class A { private count = 0; }",
			expected: "import * as sheker from \"sheker\"; " +
				"export class A { private a = 0; }"
		});
	});

	suite.test("When class has private READONLY property, mangle them.", test => {
		runTestCase({
			input: "export class A { private readonly foo=1; " +
				"constructor(){ console.log(this.foo); } }",
			expected: "export class A { private readonly a=1; " +
				"constructor(){ console.log(this.a); } }"
		});
	});

	suite.test("When class has private functions, then mangle them.", test => {
		runTestCase({
			input: "export class C { private foo(arg: number): boolean {} this.foo();" +
				" private static boo(){} A.boo(); }",
			expected: "export class C { private a(arg: number): boolean {} this.a();" +
				" private static boo(){} A.boo(); }"
		});
	});

	suite.test("When class has private ASYNC functions, then mangle them.", test => {
		// Static again. Why no mangle?
		runTestCase({
			input: "export class C { private async foo(){} this.foo(); private static async boo(){} A.boo(); }",
			expected: "export class C { private async a(){} this.a(); private static async boo(){} A.boo(); }"
		});
	});

	suite.test("When class has private and public properties, only mangle private.", test => {
		runTestCase({
			input: "export class D { private foo; private fooB; public fooC; this.foo; this.fooB; this.fooC; }",
			expected: "export class D { private a; private b; public fooC; this.a; this.b; this.fooC; }"
		});
	});

	suite.test("When processing deriving class, no mangling occurs.", test => {
		// Will cause a typescript error because both classes will
		// probably have a property named 'a' of different types.
		// Typescript raises an error because at runtime both classes will
		// use this property for different purposes and will expect different data.
		runTestCase({
			input: "export class F extends F1 { private foo; constructor(){ this.foo = new Foo(); } }",
			expected: "export class F extends F1 { private foo; constructor(){ this.foo = new Foo(); } }"
		});
	});

	suite.test("2 classes.", test => {
		runTestCase({
			input: "class A { private count=0; private name='a'; " +
				"this.count++; this.count++ \n this.name='b'; } " +
				"class B { public count=0; this.count++; }",
			expected: "class A { private a=0; private b='a'; " +
				"this.a++; this.a++ \n this.b='b'; } " +
				"class B { public count=0; this.count++; }"
		});
	});

	suite.test("When member is in an if with '&&', '||', '()', then mangle it.", test => {
		runTestCase({
			input:
				"export class A { private foo;\n" +
				"constructor(){ if(this.foo == this.foo&&this.foo||this.foo&&(this.foo)){} } }",
			expected:
				"export class A { private a;\n" +
				"constructor(){ if(this.a == this.a&&this.a||this.a&&(this.a)){} } }",
		});
	});
}